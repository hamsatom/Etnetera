package cz.etn.etnshop.template.dao;


import cz.etn.etnshop.impl.model.Product;

public interface ProductDAO extends DAO<Product> {

}
