package cz.etn.etnshop.template.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nonnull;
import org.hibernate.SessionFactory;

/**
 * @author Tomáš Hamsa
 */
public interface DAO<T extends Serializable> {

  void setSessionFactory(@Nonnull final SessionFactory sessionFactory);

  @Nonnull
  List<T> list();

  @Nonnull
  Optional<Serializable> save(@Nonnull final T entity);

  @Nonnull
  Optional<T> find(@Nonnull final Serializable id);

  @Nonnull
  Optional<T> update(@Nonnull final T entity);
}
