package cz.etn.etnshop.template.service;


import cz.etn.etnshop.impl.model.Product;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.transaction.annotation.Transactional;


public interface ProductService {

  @Transactional(readOnly = true)
  @Nonnull
  List<Product> listProducts();

  @Transactional
  @Nullable
  Integer createProduct(@Nonnull final Product product);

  @Transactional
  @Nullable
  Integer updateProduct(@Nonnull final Product product);

  @Transactional(readOnly = true)
  @Nullable
  Product get(@Nonnull final Serializable id);
}
