package cz.etn.etnshop.impl.controller;

import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public final class WelcomeController {

  @RequestMapping(value = "/")
  public String index(Map<String, Object> ignored) {

    return "index";
  }

}