package cz.etn.etnshop.impl.controller;

import cz.etn.etnshop.impl.dto.IdDTO;
import cz.etn.etnshop.impl.model.Product;
import cz.etn.etnshop.template.service.ProductService;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController(value = "productController")
@RequestMapping("/product")
public final class ProductController {

  private static final Logger logger = Logger.getLogger(ProductController.class.getName());
  private final ProductService productService;

  @Autowired
  public ProductController(@Nonnull final ProductService productService) {
    this.productService = productService;
  }

  @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Nonnull
  public IdDTO create(@RequestBody final Product product) {
    logger.info(() -> String.format("Creating product %s", product));

    return new IdDTO(productService.createProduct(product));
  }

  @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Nonnull
  public List<Product> list() {
    logger.info(() -> "Listing all products");

    return productService.listProducts();
  }

  @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Nullable
  public Product get(@PathVariable("id") final int id) {
    logger.info(() -> String.format("Getting product id=%s", id));

    return productService.get(id);
  }

  @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Nonnull
  public IdDTO update(@RequestBody final Product product) {
    logger.info(() -> String.format("Updating product=%s", product));

    return new IdDTO(productService.updateProduct(product));
  }

}
