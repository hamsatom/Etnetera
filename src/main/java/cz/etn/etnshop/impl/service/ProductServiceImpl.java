package cz.etn.etnshop.impl.service;

import cz.etn.etnshop.impl.model.Product;
import cz.etn.etnshop.template.dao.ProductDAO;
import cz.etn.etnshop.template.service.ProductService;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("productService")
public final class ProductServiceImpl implements ProductService {

  private static final Logger logger = Logger.getLogger(ProductServiceImpl.class.getName());

  private final ProductDAO productDAO;

  @Autowired
  public ProductServiceImpl(@Nonnull final ProductDAO productDAO) {
    this.productDAO = productDAO;
  }

  @Nonnull
  @Override
  public List<Product> listProducts() {
    logger.info("Listing all products");

    return productDAO.list();
  }

  @Nullable
  @Override
  public Integer createProduct(@Nonnull final Product product) {
    logger.info(() -> String.format("Creating product %s", product));

    final Optional<Serializable> savedProduct = productDAO.save(product);

    return (Integer) savedProduct.orElse(null);
  }

  @Nullable
  @Override
  public Integer updateProduct(@Nonnull final Product product) {
    logger.info(() -> String.format("Updating product %s", product));

    final Optional<Product> updatedProduct = productDAO.update(product);

    return updatedProduct.isPresent()
        ? updatedProduct.get().getId()
        : null;
  }

  @Nullable
  @Override
  public Product get(@Nonnull final Serializable id) {
    logger.info(() -> String.format("Getting product with id %s", id));

    return productDAO.find(id).orElse(null);
  }
}
