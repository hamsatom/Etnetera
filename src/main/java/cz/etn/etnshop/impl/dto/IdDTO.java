package cz.etn.etnshop.impl.dto;

import java.util.Objects;

/**
 * @author Tomáš Hamsa on 23.09.2017.
 */
public class IdDTO {

  private final String id;

  public IdDTO(final Object id) {
    this.id = Objects.toString(id, "");
  }

  public String getId() {
    return id;
  }
}
