package cz.etn.etnshop.impl.dao;


import cz.etn.etnshop.template.dao.DAO;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tomáš Hamsa
 */
@SuppressWarnings("unchecked")
abstract class AbstractDAO<T extends Serializable> implements DAO<T> {

  private static final Logger LOG = Logger.getLogger(AbstractDAO.class.getName());
  private final Class<T> type;
  private SessionFactory sessionFactory;

  AbstractDAO() {
    final Type t = getClass().getGenericSuperclass();
    final ParameterizedType pt = (ParameterizedType) t;
    type = (Class) pt.getActualTypeArguments()[0];

    LOG.fine(() -> String.format("Created DAO for type %s", type));
  }

  @Override
  @Autowired
  public void setSessionFactory(@Nonnull final SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Nonnull
  @Override
  public List<T> list() {
    LOG.info(() -> String.format("Listing all %s", type));

    final Session session = sessionFactory.getCurrentSession();
    final Criteria criteria = session.createCriteria(type);

    return (List<T>) criteria.list();
  }

  @Nonnull
  @Override
  public Optional<Serializable> save(@Nonnull final T entity) {
    LOG.info(() -> String.format("Registering %s", entity));

    final Session session = sessionFactory.getCurrentSession();
    final Serializable id = session.save(entity);
    session.flush();

    return Optional.ofNullable(id);
  }

  @Nonnull
  @Override
  public Optional<T> find(@Nonnull final Serializable id) {
    LOG.info(() -> String.format("Looking for entity with id %s", id));

    final Session session = sessionFactory.getCurrentSession();

    return Optional.ofNullable((T) session.get(type, id));
  }

  @Nonnull
  @Override
  public Optional<T> update(@Nonnull final T entity) {
    LOG.info(() -> String.format("Updating %s", entity));

    final Session session = sessionFactory.getCurrentSession();
    final Object updatedEntity = session.merge(entity);
    session.flush();

    return Optional.ofNullable((T) updatedEntity);
  }

}
