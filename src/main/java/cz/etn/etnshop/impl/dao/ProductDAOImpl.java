package cz.etn.etnshop.impl.dao;

import cz.etn.etnshop.impl.model.Product;
import cz.etn.etnshop.template.dao.ProductDAO;
import org.springframework.stereotype.Repository;

@Repository("productDAO")
public final class ProductDAOImpl extends AbstractDAO<Product> implements ProductDAO {

}
