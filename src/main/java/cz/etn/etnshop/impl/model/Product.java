package cz.etn.etnshop.impl.model;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product implements Serializable {

  private static final long serialVersionUID = -2739622030641073946L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "serialNumber", nullable = false, unique = true)
  private String serialNumber;

  public int getId() {
    return id;
  }

  public Product setId(final int id) {
    this.id = id;
    return this;
  }

  public String getName() {
    return name;
  }

  public Product setName(@Nonnull final String name) {
    this.name = name;
    return this;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public Product setSerialNumber(@Nonnull final String serialNumber) {
    this.serialNumber = serialNumber;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Product)) {
      return false;
    }
    Product product = (Product) o;
    return id == product.id &&
        Objects.equals(name, product.name) &&
        Objects.equals(serialNumber, product.serialNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, serialNumber);
  }

  @Override
  public String toString() {
    return "Product{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", serialNumber='" + serialNumber + '\'' +
        '}';
  }
}
