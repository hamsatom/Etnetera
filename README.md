Implementace testovacího úkolu.
Backend podporuje read, update a create produktu a listování všech produktů.
Změněno mapování listování všeho na GET /product míst /product/list
ProductController by měl přijímat a vracet JSON.

Změněn DAO pattern na generický s vracením Optional, aby bylo nutno testovat zda
se něco vrátilo a nenastával NullPointerException.
Neměnil jsem Session.getCurrentSession() i když normálně by se měl asi používat
EnitityManager a nechtěl jsem řešit persistence.xml.
Neimplementoval jsem delete operaci které jediná z CRUDu chybí, protože to ze 
zadání nebylo potřeba.

Přidáno sériové číslo do Productu jako sloupec nenullových unikátní stringů.
Adekvátně upravena tabulka v create scriptu, kde nebyl sloupec name jako NOT NULL 
a defaultní hodnota byla NULL, i když entita Product má name jako
@Column(nullable = false).

V projektu celkově upravena struktura na oddělení interfaců a implementace,
změněno autowirování přes field na čistší autowirování přes konstruktor 
nebo setter. Přidána závislost na Findbugs-JSR305 kvůli @Nonnull a @Nullable.

Neměnil jsem frontend, protože se hlásím na pozici java backendisty, takže 
javascriptu nerozumím a nechci frontendistům fušovat do řemesla.